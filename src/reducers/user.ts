import { IUser, Action, ActionTypes } from '../actions';

export const userReducer = (state: IUser[] = [], action: Action) => {
  switch (action.type) {
    case ActionTypes.fetchUser:
      return action.payload;
    default:
      return state;
  }
};
