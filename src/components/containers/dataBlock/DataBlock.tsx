import React from 'react';

interface IDataBlock {
  children?: any;
  title: string;
  className?: string;
}

const DataBlock = (props: IDataBlock) => {
  return (
    <div className={props.className}>
      <h3 className='title'>{props.title}</h3>
      {props.children}
    </div>
  );
};

export default DataBlock;
