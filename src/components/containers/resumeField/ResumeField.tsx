import React from 'react';

export interface IResumeField {
  children?: any;
  left_side?: any;
  right_side?: any;
  border?: string;
}

const ResumeField = (props: IResumeField) => {
  if (props.children) {
    return (
      <div className={`resume-field ${props.border}`}>{props.children}</div>
    );
  }
  return (
    <div className={`resume-field ${props.border}`}>
      <div className='resume-field-left'>{props.left_side}</div>
      <div className='resume-field-right'>{props.right_side}</div>
    </div>
  );
};

export default ResumeField;
