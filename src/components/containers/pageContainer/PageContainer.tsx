import React from 'react';
import './pageContainer.sass';

class PageContainer extends React.Component {
  render() {
    return (
      <>
        <div className='page-container'>
          <div className='black-list bl1' />
          <div className='black-list bl2' />
          <div className='white-list wl1' />
          <div className='white-list wl2' />
          <div className='bg-white main-content'>{this.props.children}</div>
        </div>
      </>
    );
  }
}

export default PageContainer;
