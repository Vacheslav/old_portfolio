import React from 'react';

const InlineError = ({ text }: { text: string }) => (
  <span className='inline-error'>{text}</span>
);

export default InlineError;
