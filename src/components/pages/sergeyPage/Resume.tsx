import React, { Component } from 'react';
import ResumeField from '../../containers/resumeField';
import Personals from '../resumeBlocks/Personals';
import { withTranslation } from 'react-i18next';
import Links from '../resumeBlocks/Links';
import Skills from '../resumeBlocks/Skills';
import Languages from '../resumeBlocks/Languages';
import WorkExperience from '../resumeBlocks/WorkExperience';
import Education from '../resumeBlocks/Education';
import Certificates from '../resumeBlocks/Certificates';
import Projects from './../mainPage/Projects';

interface IResumeProps {
  t: any;
  i18n: any;
}

class Resume extends Component<IResumeProps> {
  render() {
    return (
      <>
        <ResumeField
          border={'bottom'}
          left_side={
            <Personals
              data={this.props.t('resume data')}
              titles={this.props.t('resume_blocks')}
              className='resume-data__block'
              photo='./images/sergey/Sergey.jpg'
            ></Personals>
          }
          right_side={
            <Links className='resume-data__block links' d_links={true} />
          }
        />
        <ResumeField border={'none'}>
          <div className='objective'>
            {this.props.t('Career objective')}:{' '}
            <span className='objective-text'>
              {this.props.t('objective text')}
            </span>
          </div>
        </ResumeField>
        <ResumeField
          border={'top'}
          left_side={<Skills />}
          right_side={<Languages />}
        />
        <ResumeField
          border={'top'}
          left_side={<WorkExperience className='work-experience' l={250} />}
          right_side={<Education className='education-block' />}
        />
        <ResumeField border={'border-top'}>
          <Certificates className='certificates-block' q={30} />
        </ResumeField>
        <ResumeField border={'border-top'}>
          <Projects />
        </ResumeField>
      </>
    );
  }
}
//@ts-ignore
export default withTranslation()(Resume);
