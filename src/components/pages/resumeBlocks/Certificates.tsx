import React from 'react';
import DataBlock from '../../containers/dataBlock';
import { useTranslation } from 'react-i18next';
import { IPageData } from '../mainPage/interfaces';
import { Link } from 'react-router-dom';

interface ICertificateProps {
  className: string;
  q: number;
}

const Certificates = (props: ICertificateProps) => {
  const { t } = useTranslation();
  let titles: {
      certificates: { title: string };
    } = t('resume_blocks'),
    data: IPageData = t('resume data');
  return (
    <DataBlock title={titles.certificates.title} className={props.className}>
      <ul className='certificates'>
        {data.certificates.map((el, idx) => {
          if (idx < props.q) {
            return (
              <li key={idx}>
                <span className='course__name'>{el.name}</span> <br /> by{' '}
                <span className='course__by'>{el.by}</span>
              </li>
            );
          }
          if (idx === props.q) {
            return (
              <li key={idx}>
                <p>...</p>
                <Link to='/sergey'>more in resume</Link>
              </li>
            );
          } else {
            return null;
          }
        })}
      </ul>
    </DataBlock>
  );
};

export default Certificates;
