import React from 'react';
import DataBlock from '../../containers/dataBlock/DataBlock';
import { useTranslation } from 'react-i18next';

interface ILinksProps {
  className: string;
  d_links?: boolean;
}

const Links = (props: ILinksProps) => {
  const { t } = useTranslation();
  return (
    <>
      <DataBlock title={t('Links')} className={props.className}>
        <div className='links-list'>
          <a href='https://vk.com/seregashlykov'>VK</a>
          <a href='https://github.com/SShlykov'>Github</a>
          <a href='https://github.com/SShlykov'>LinkedIn</a>
          <a href='https://twitter.com/sergey_shlykov'>Twitter</a>
        </div>
      </DataBlock>
      {props.d_links ? (
        <DataBlock title={t('download resume')} className={props.className}>
          <ul>
            <li>
              <span className='lang'>EN</span>
              <span className='doctype'>
                <a href='#in_future'>docx</a>
                <a href='#in_future'>pdf</a>
              </span>
            </li>
            <li>
              <span className='lang'>RU</span>
              <span className='doctype'>
                <a href='./docs/SShlykov_resume_ru.doc'>doc</a>
                <a href='./docs/SShlykov_resume_ru.pdf'>pdf</a>
              </span>
            </li>
          </ul>
        </DataBlock>
      ) : (
        ''
      )}
    </>
  );
};

export default Links;
