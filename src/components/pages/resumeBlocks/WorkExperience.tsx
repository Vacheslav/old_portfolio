import React from 'react';
import DataBlock from '../../containers/dataBlock';
import { useTranslation } from 'react-i18next';
import { IPageData } from '../mainPage/interfaces';

const WorkExperience = (props: { className: string; l: number }) => {
  const { t } = useTranslation();
  let titles: { work_experience: { title: string } } = t('resume_blocks'),
    data: IPageData = t('resume data');
  let l = props.l;
  return (
    <DataBlock title={titles.work_experience.title} className={props.className}>
      <ul>
        {data.workexperience.map((el, idx) => {
          return (
            <li className='work-experience-item' key={`we-${idx}`}>
              <span className='work-experience-item__place'>
                {`${el.position[0].toUpperCase()}${el.position.slice(1)} ${t(
                  'at'
                )} ${el.at}`}
              </span>
              <span className='work-experience-item__dates'>
                {`${el.from} - ${el.to}`}
              </span>
              <div className='work-experience-item-tasks'>
                {el.tasks
                  .filter((el, idx) => {
                    if (idx < 3) {
                      return el;
                    } else {
                      return null;
                    }
                  })
                  .map((el, idx) => {
                    return (
                      <p
                        className='work-experience-item-tasks__task'
                        key={`wet-${idx}`}
                      >
                        {el.length > l ? el.slice(0, l - 3).concat('...') : el}
                      </p>
                    );
                  })}
              </div>
            </li>
          );
        })}
      </ul>
    </DataBlock>
  );
};

export default WorkExperience;
