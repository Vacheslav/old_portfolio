import React from 'react';
import DataBlock from '../../containers/dataBlock';
import { useTranslation } from 'react-i18next';

const Languages = () => {
  const { t } = useTranslation();
  return (
    <DataBlock title={t('Languages')}>
      <div className='language'>
        <div className='language__title'>{t('English')}</div>
        <div className='language__level'>{t('Eng_level')}</div>
      </div>
    </DataBlock>
  );
};

export default Languages;
