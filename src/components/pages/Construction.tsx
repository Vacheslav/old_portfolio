import React from 'react';
import { useTranslation } from 'react-i18next';
import LangSwitcher from './../../utils/LangSwitcher';

const Construction = () => {
  const { t } = useTranslation();
  return (
    <>
      <div
        className='sorry_under_construction'
        dangerouslySetInnerHTML={{ __html: t('sorry under construction') }}
      ></div>
      <ul>
        <LangSwitcher />
      </ul>
    </>
  );
};

export default Construction;
