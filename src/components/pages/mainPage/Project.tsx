import React from 'react';
import { useTranslation } from 'react-i18next';

export interface IProject {
  id: number;
  name: string;
  stack: {
    front: string[];
    back: string[];
  };
  date: string;
  objectives: string;
  images: {
    img1: any;
    img2: any;
    img3: any;
  };
}

const Tech = (props: { text: string }) => {
  return <div className='tech'>{props.text}</div>;
};

const Project = (project: IProject) => {
  const { t } = useTranslation();
  return (
    <div className='project'>
      <div className='project-data-wrapper'>
        <div className='project__name'>{project.name}</div>
        <div className='project__release-date'>{project.date}</div>
        <div className='project-stack'>
          <h4>Stack: </h4>
          <div className='project-stack__block'>
            {project.stack.front.map((el, idx) => {
              return <Tech text={el} key={`tech-1-${idx}`} />;
            })}
          </div>
          <div className='project-stack__block'>
            {project.stack.back.map((el, idx) => {
              return <Tech text={el} key={`tech-1-${idx}`} />;
            })}
          </div>
        </div>
        <div className='project__objective'>
          <h4>{t('Project objective')}:</h4>
          <p>{project.objectives}</p>
        </div>
      </div>
      <div className='project-images-wrapper'>
        <div className='main-image-wrapper'>
          <figure className='img img1'>
            <img src={project.images.img1} alt='' />
          </figure>
        </div>
        <div className='two-images-block'>
          <figure className='img img2'>
            <img src={project.images.img3} alt='' />
          </figure>
          <figure className='img img3'>
            <img src={project.images.img2} alt='' />
          </figure>
        </div>
      </div>
    </div>
  );
};

export default Project;
