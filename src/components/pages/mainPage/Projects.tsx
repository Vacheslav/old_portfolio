import React from 'react';
import Project, { IProject } from './Project';
import { useTranslation } from 'react-i18next';

const Projects = () => {
  const { t } = useTranslation();
  let projects: IProject[] = t('projects');
  return (
    <div className='projects' id='projects'>
      <div className='big-text bg'>Projects</div>
      <h2>IDEAS.</h2>
      <div className='projects-block'>
        <Project {...projects[0]} />
        <Project {...projects[1]} />
        <Project {...projects[2]} />
      </div>
      <div className='to-be-continued'>to be continued...</div>
    </div>
  );
};

export default Projects;
