import { string } from 'prop-types';

interface IPersonals {
  name: string;
  age: string;
  locale: string;
  email: string;
  phone: string;
}
interface IDegree {
  master: string;
  bachelor: string;
}
interface ICertificate {
  name: string;
  by: string;
}
interface IWorkExperience {
  position: string;
  at: string;
  from: string;
  isOfficial: boolean;
  to: string;
  tasks: string[];
}
interface ISkills {
  design: string[];
  frontend: string[];
  backend: string[];
  other: string[];
}

export interface IPageData {
  id: number;
  personals: IPersonals;
  degree: IDegree;
  certificates: ICertificate[];
  skills: ISkills;
  workexperience: IWorkExperience[];
  photo: any;
}
