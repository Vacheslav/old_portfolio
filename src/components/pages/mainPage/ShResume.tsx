import React from 'react';
import { Link } from 'react-router-dom';

import { withTranslation } from 'react-i18next';
import { IPageData } from './interfaces';
import DataBlock from '../../containers/dataBlock';
import Personals from '../resumeBlocks/Personals';
import Links from '../resumeBlocks/Links';
import WorkExperience from '../resumeBlocks/WorkExperience';
import Education from '../resumeBlocks/Education';
import Certificates from '../resumeBlocks/Certificates';

interface IShResumeProps {
  t: any;
  i18n: any;
}

class ShortResume extends React.Component<IShResumeProps> {
  render() {
    const data: IPageData = this.props.t('resume data'),
      titles = this.props.t('resume_blocks');
    return (
      <div className='shresume' id='resume'>
        <div className='big-text'>about me</div>
        <h2>resume.</h2>
        <div className='photo'>
          <img src={data.photo} alt='' />
        </div>
        <Link to='/sergey' style={{ textAlign: 'center' }}>
          <span className='underline'>{this.props.t('link to full')}</span>{' '}
          <br /> {this.props.t('resume')}
        </Link>
        <div className='shresume-data'>
          <Personals
            data={data}
            titles={titles}
            className='shresume-data__block'
          />
          <Education className='shresume-data__block' />
          <DataBlock
            title={titles.skills.title}
            className='shresume-data__block skills'
          >
            <p
              style={{
                fontSize: '13px',
                lineHeight: '16px',
                fontWeight: 'bold'
              }}
            >
              Main stack:
            </p>{' '}
            <p style={{ fontSize: '12px', color: '#686868' }}>
              Typescript, React, Redux
            </p>
            <p style={{ fontSize: '12px', color: '#686868' }}>Laravel api</p>
          </DataBlock>
          <Certificates className='shresume-data__block' q={3} />
          <WorkExperience
            className='shresume-data__block work-experience'
            l={70}
          />
          <Links className='shresume-data__block full-width links' />
        </div>
      </div>
    );
  }
}

//@ts-ignore
export default withTranslation()(ShortResume);
