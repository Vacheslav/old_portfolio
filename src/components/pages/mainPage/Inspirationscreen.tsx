import React from 'react';
import { useTranslation } from 'react-i18next';

const Inspirationscreen = () => {
  const { t } = useTranslation();
  return (
    <div className='inspscreen'>
      <div className='big-text bg'>Inspiration</div>
      <h2>message.</h2>
      <p dangerouslySetInnerHTML={{ __html: t('inspiration text') }}></p>
    </div>
  );
};

export default Inspirationscreen;
