import React from 'react';
import { Link } from 'react-router-dom';

const NotFound = () => {
  return (
    <div className='not-found'>
      <h1>404</h1>
      <h2>Page not found</h2>
      <div>
        try to turn back to{' '}
        <Link to='/' style={{ textDecoration: 'underline' }}>
          main page
        </Link>
      </div>
    </div>
  );
};

export default NotFound;
