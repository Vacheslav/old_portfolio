import { Dispatch } from 'redux';
import { ActionTypes } from './types';
import { IFormData } from '../components/forms/feedbackForm/FeedbackForm';
import axios from 'axios';
const __url = 'https://';

export interface SandMailAction {
  type: ActionTypes.sandMail;
  payload: string;
}

export const sendToMail = (data: IFormData, email: string) => {
  return async (dispatch: Dispatch) => {
    const response: string = await axios
      .post<any, string>(`${__url}/send-email`, {
        ...data,
        email
      })
      .then(req => {
        console.log(req);
        return req;
      })
      .catch(err => {
        console.warn(err.message);
        return 'message not send cause of error';
      });

    dispatch<SandMailAction>({
      type: ActionTypes.sandMail,
      payload: response
    });
  };
};
