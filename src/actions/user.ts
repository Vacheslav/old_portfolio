/* import axios from 'axios';
const __url = 'https://'; */

import { Dispatch } from 'redux';
import { ActionTypes } from './types';

export interface IUser {
  id: number;
}

export interface FetchUserAction {
  type: ActionTypes.fetchUser;
  payload: IUser[];
}

export const fetchUser = () => {
  return (dispatch: Dispatch) => {
    /* const response = await axios.get<ITour[]>(__url/User/); */
    const response: { data: IUser[] } = {
      data: []
    };

    dispatch<FetchUserAction>({
      type: ActionTypes.fetchUser,
      payload: response.data
    });
  };
};
